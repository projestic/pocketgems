# Pocket Gems

Public website for Pocket Gems.

The website uses Wordpress for CMS management and site structure.

# Setup

## Pre-requisites

The pre-recquisites are listed for a Mac development environment. This setup cross-referenced the [installation instructions](https://codex.wordpress.org/Installing_WordPress_Locally_on_Your_Mac_With_MAMP) given by WordPress and [this tutorial](http://egalo.com/2012/05/15/clone-live-wordpress-to-local-env/).

1. [Install MAMP](http://www.mamp.info/en/downloads/index.html)

## Running the site with an existing database

1. Open and launch MAMP
1. Under the **Web Server** tab or on the main screen for MAMP Pro, set the document root to point to this downloaded GitHub repo stored locally on your machine.
1. Save settings.
1. Click **start servers** from the MAMP screen.
1. MAMP should launch a page immediately. If not, click **open start page** or **WebStart**. If neither work, visit the URL `http://localhost:8888`.
1. Once the page is open, select **phpMyAdmin** from the webpage.
1. Click **Databases** and then **Create new database**. Enter in a database name such as "pocketgems," and press "Create." No need to choose an option for "collation:" it will automatically be assigned by MySQL when the database tables are created, during the WordPress installation. Currently, Pocket Gems databases are stored in GitHub using a date-naming convention, such as `2017_08_17_pocketgems.sql` and you may want to match that.
1. Download a copy of the site database from this GitHub repo. Those can be found at [https://github.com/pocketgems/pocketgems-website/tree/master/sql](https://github.com/pocketgems/pocketgems-website/tree/master/sql). Save that file to your desktop. You will import it into the new database your created on phpMyAdmin.
1. Click the **Import** tab on phpMyAdmin.
1. Click **Choose file**. Locate the file you saved from the step above to your desktop.
1. CLick **Go**.
1. The file is being uploaded at this point. The import should compelte successfully.
1. Now, you can create a new user to access this database.
    1. Open the local phpMyAdmin provided by MAMP.
    1. Select the WordPress database in phpMyAdmin’s left-hand navigation bar.
    1. Click Privileges.
    1. Click Add User.
    1. Enter a username, or paste the username from the DB_USER field in wp-config.php, into the User name field in this form.
    1. Select “Local” from the Host drop-down list (this bit is important!)
    1. Choose a new password, and enter it both in the DB_PASSWORD field in wp-config.php, and in the Password field in this form, twice. (Alternatively just copy the existing live password from wp-config.php into this form — but it’s probably better to choose a new one.)
    1. Ensure that Grant all privileges on database `<dbname>` is selected.
    1. Click Go.
1. Under the database panel on the left of phpMyAdmin, select **wp_options** (you may need to "open" the file tree by clicking the +).
1. Select the **Browse** tab
1. The first two rows you see called **siteurl** and **home** need to be edited.
1. Click the **Edit** button for the first row, called **siteurl**
1. Change the value to `http://localhost:8888` under **option_value**
1. Click **Go** to save it.
1. Repeat for the row called **home**.
1. Open the GitHub repo stored locally on your machine in your preferred IDE (text editor).
1. Locate the .htaccess file (this can sometimes be hidden by your file browser)
1. Change the line RewriteBase to read `RewriteBase /`
1. Change the line RewriteRule to read `RewriteRule . /index.php [L]`
1. Save this file
1. Check the file called `wp_config.php` to make sure the username, database, and password match what you have created.
1. Stop and restart your MAMP servers
1. Navigate your browser to `http://localhost:8888`. The site should load properly.
1. You can access the admin backend at `http://localhost:8888/wp-admin/` with the username and password `admin`.
1. **Note:** Since you have changed your local copies of `wp-config.php` and `.htaccess`, it is important **not** to commit these local files to GitHub.

## Running SCSS

1. [Install Node](https://nodejs.org/en/) using the official LTS (long term support) package.
1. [Install Gulp](https://gulpjs.com) as well.
1. Run `npm install --save-dev`
1. Run `gulp build` to generate SCSS once
1. Run `gulp watch` to generate new CSS as the old ones change.