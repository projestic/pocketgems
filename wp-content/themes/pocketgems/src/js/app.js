/*
 Third party
 */

//= ../vendor/jquery/dist/jquery.min.js
//= ../vendor/owl.carousel/dist/owl.carousel.min.js

//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/tab.js
//= ../vendor/bootstrap-sass/assets/javascripts/bootstrap/transition.js

$(function(){

    $('.bt-menu-trigger').on('click', function(e){
        e.preventDefault();
        $('html').toggleClass('mobile-menu-open');
        $('#header .nav-holder').slideToggle();
    });

    $('.hero-slider').owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: false,
        nav: true,
        onInitialized: function(e) {
            $(e.target).find('video').get(0).play();
        }
    });

    $('[href="#tabAll"]').on('click',function(e){
        e.preventDefault();
        var $this = $(this),
            tabset = $this.closest('.tabset');

        tabset.addClass('show-all');

        $this.parent().addClass('active');
        tabset.find('.tab-pane').addClass('active in');
        tabset.find('[data-toggle="tab"]').parent().removeClass('active');
    });

    $('.tab-menu > li:not(:first-child) > a').on('click', function(){
        $(this).closest('.tabset').removeClass('show-all');
    });

    $('[href="#empty-link"]').on('click',function(e){
        e.preventDefault();
    });

    var heroSliderVideos = $('.hero-slider video');
    heroSliderVideos && setHeroSliderHeight(heroSliderVideos);

    function setHeroSliderHeight(videos){
        videos.each(function(){
            var $this = $(this);

            $this.closest('.hero-slider').addClass('has-video');
        });
    }

    function setDropdownDirection(submenu){

        submenu.each(function(){
            var $this = $(this),
                expandClass = ($this.width() + $this.parent().offset().left) < $(window).width() ? 'expand-right' : 'expand-left';
            $this.removeClass('expand-left expand-right').addClass(expandClass);
        });
    }

    var subMenu = $('.sub-menu');
    subMenu && setDropdownDirection(subMenu);

    $(window).on('resize', function(){
        subMenu && setDropdownDirection(subMenu);
        $('.hero-slider video') && setHeroSliderHeight($('.hero-slider video'));
    });
    //Read more/read less btn
    $('.read-more').on('click', function(e){
        e.preventDefault();
    $(this).hasClass('active') ? $(this).prev().hide(200) && $(this).text('Read-more') && $(this).removeClass('active') : $(this).prev().show(200) && $(this).text('Read-less') && $(this).addClass('active');
    });

    //Due to issue on android devices when one click on menu item prevents you access to dropdown sub items. Prevents immidiately redirection on mobile devices on one click menu item that has dropdowns. Now needed doubleclick to accessit.
    $.fn.doubleTapToGo = function( params ) {
        if( !( 'ontouchstart' in window ) &&
            !navigator.msMaxTouchPoints &&
            !navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

        this.each( function()
        {
            var curItem = false;

            $( this ).on( 'click', function( e )
            {
                var item = $( this );
                if( item[ 0 ] != curItem[ 0 ] )
                {
                    e.preventDefault();
                    curItem = item;
                }
            });

            $( document ).on( 'click touchstart MSPointerDown', function( e ) {
                var resetItem = true,
                    parents   = $( e.target ).parents();

                for( var i = 0; i < parents.length; i++ )
                    if( parents[ i ] == curItem[ 0 ] )
                        resetItem = false;

                if( resetItem )
                    curItem = false;
            });
        });
        return this;
    };
    $( '#menu-main-menu li:has(ul)' ).doubleTapToGo();

    //Sticky menu after get riched bottom of the viewport
    $(window).scroll(function () {
        var elementOffset = $(window).scrollTop();
        var viewportHeight = $(window).height();
        var elementHeight = $('#header').height();
        if (viewportHeight < elementOffset) {
            //$( ".header" ).slideUp( "hide", function() {});
            //$('#header').addClass('header-sticky');
            //$("#header").addClass("cus-header-sticky");
            //$("#header").css('top','30px');
         } else if (elementOffset < $('#header').height()) {
            $( ".header" ).slideDown( "slow", function() {});
           //$('#header').removeClass('header-sticky');
            //$("#header").removeClass("cus-header-sticky");
            //$("#header").css('top','10px');
         }
    });



    //Popover on bootstrap and tether library https://v4-alpha.getbootstrap.com/components/popovers/
    //Popup windows for content people grid
    var itemPeopleGrid = $('.item');
    var popupPeopleGrid = $('.user-popup');
    var popupPeopleGridActive = $('.user-popup-active');
    var closePopup = $('.close-icon');
    itemPeopleGrid.each(function (el, i) {

        $(this).children('.close-icon').on('click', function () {
            $(this).hasClass('active-user-item') ? $(this).children('.user-popup').removeClass('user-popup-active') && $(this).removeClass('active-user-item') && $(this).children('.user-popup').hide() : $(this).children('.user-popup').addClass('user-popup-active') && $(this).addClass('active-user-item') && $(this).children('.user-popup').show();
        });
        // $('.user-popup').hide();
    });

    $(document).on('click', '.item', function () {
        if(!$(event.target).closest('.user-popup').length) {
            $(this).find('.user-popup').addClass('user-popup-active');
            $('body').addClass('init_popup');
        }
    });
    $(document).on('click', '.close-icon', function () {
        $(this).closest('.item').find('.user-popup').removeClass('user-popup-active');
        $('.user-popup-active').removeClass('user-popup-active');
        $('body').removeClass('init_popup');
    });

    //Init video gallery
    $('.video-carousel').each(function(){
        var gallery = $(this),
            isGallery = !!(gallery.data('slide-count') - 1);

        gallery.on('initialized.owl.carousel', function(e) {
            setTimeout(setOwlNavsPosition, 0);
            setOwlVideoPreview($(e.target));
        }).owlCarousel({
            center: true,
            items: 1,
            lazyLoad: false,
            loop: isGallery,
            margin: 0,
            mouseDrag: false,
            nav: false,
            stagePadding: 0,
            video: true,
            responsive: {
                768: {
                    margin: 20,
                    nav: true,
                    stagePadding: isGallery ? 150 : 0,
                },
                992: {
                    margin: 30,
                    nav: true,
                    stagePadding: isGallery ? 200 : 0,
                },
                1200: {
                    margin: 40,
                    nav: true,
                    stagePadding: isGallery ? 250 : 0,
                }
            }
        }).on('resized.owl.carousel', function(e) {
            setOwlNavsPosition($(e.currentTarget));
        });

        function setOwlNavsPosition() {

            $('.video-carousel').each(function(){
                var $dots = $(this).find('.owl-dots'),
                    $nav = $(this).find('.owl-nav'),
                    h = $(this).find('.active .owl-video-wrapper').innerHeight();

                if ($nav.length) {
                    $nav.css({
                        'top': h/2
                    });
                }

                if ($dots.length) {
                    $dots.css({
                        'top': h
                    });
                }
            });
        }

        function setOwlVideoPreview(gallery) {
            gallery.find('.item-video').each(function(){

                var customUrl = $(this).attr('data-video-preview'),
                    $owlTn = $(this).find('.owl-video-tn');

                    console.log(customUrl,$owlTn);

                if (customUrl && $owlTn.length) {
                    $owlTn.css({
                        'background-image': 'url(' + customUrl + ')'
                    });
                }
            });
        }
    });
});
