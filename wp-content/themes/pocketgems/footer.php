            <?php 
                $facebook = get_field('facebook', 'options');
                $instagram = get_field('instagram', 'options');
                $linkedin = get_field('linkedin', 'options');
                $twitter = get_field('twitter', 'options');
            ?>

            <?php if($facebook || $instagram || $linkedin || $twitter): ?>
                <div class="container">
                    <ul class="socials">

                        <?php if($twitter): ?>
                            <li>
                                <a class="fa fa-twitter" href="<?php echo $twitter; ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>

                        <?php if($facebook): ?>
                            <li>
                                <a class="fa fa-facebook" href="<?php echo $facebook; ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>

                        <?php if($instagram): ?>
                            <li>
                                <a class="fa fa-instagram" href="<?php echo $instagram; ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>

                        <?php if($linkedin): ?>
                            <li>
                                <a class="fa fa-linkedin" href="<?php echo $linkedin ?>" target="_blank"></a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
            <?php endif; ?>

            <footer id="footer">
                <div class="container">
                    <?php
                        if (has_nav_menu('primary_footer_navigation')) :
                            wp_nav_menu(array(
                                'container'      => false,
                                'depth'          => 1,
                                'menu'           => 'Primary Footer Menu',
                                'menu_class'     => 'menu menu-primary',
                                'theme_location' => 'primary_footer_navigation'
                            ));
                        endif; 
                    
                        if (has_nav_menu('secondary_footer_navigation')) :
                            wp_nav_menu(array(
                                'container'      => false,
                                'depth'          => 1, 
                                'menu'           => 'Secondary Footer Menu',
                                'menu_class'     => 'menu menu-secondary',
                                'theme_location' => 'secondary_footer_navigation'
                            ));
                        endif; 
                    ?>

                    <?php 
                        $copyrigth = get_field('copyright', 'options');
                        if(!empty($copyrigth)):
                    ?>
                        <div class="copyright"><?php echo str_replace('{{year}}', date('Y'), $copyrigth); ?></div>
                    <?php endif; ?>
                </div>
            </footer>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>