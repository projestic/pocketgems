$(document).ready(function() {

    // Default dropdown action to show/hide dropdown content
    $('.js-dropp-action').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('js-open');
        $(this).parent().next('.dropp-body').toggleClass('js-open');
    });

    // Using as fake input select dropdown
    $('label').click(function() {
        $(this).addClass('js-open').siblings().removeClass('js-open');
        $('.dropp-body,.js-dropp-action').removeClass('js-open');
    });
    // get the value of checked input radio and display as dropp title
    $('.option').change(function() {
        var value = $("input[name='dropp']:checked").val();
        var mainParent = $(this).parent().parent().parent();
        mainParent.find('span').text(value);
    });
    $('.carousel-inner .active div:nth-child(2)').attr('id', 'current');
function callPlayer(frame_id, func, args='') {
        console.log(frame_id);
        if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
        var iframe = document.getElementById(frame_id);
        if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }
    if (iframe) {
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
}
$('#theCarousel').on('slide.bs.carousel', function () {
  callPlayer('current','pauseVideo');
});
$('#theCarousel').on('slid.bs.carousel', function () {
  $('.carousel-inner .item #current').attr('id', '');
  $('.carousel-inner .active div:nth-child(2)').attr('id', 'current');
});

});
