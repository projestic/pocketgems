jQuery(document).ready(function($) {

  function pocketgems_update_job_filter_text (filter_array_values, filter_wrapper_class, jobs_filter_default_text, filter_all_checkboxes_length) {
    if(filter_array_values.length== 0 || filter_array_values.length == filter_all_checkboxes_length){
      jQuery(filter_wrapper_class + ' .text-muted').empty();
			jQuery(filter_wrapper_class + ' .text-muted').append(jobs_filter_default_text);
    } else {
      var filter_text_muted = filter_array_values.join(', ').substring(0, 24);
      if(filter_text_muted.length >= 24){
        filter_text_muted += '...';
      }
      jQuery(filter_wrapper_class + ' .text-muted').empty();
			jQuery(filter_wrapper_class + ' .text-muted').append(filter_text_muted);
    }
  }

  function pocketgems_apply_job_filter () {
    var jobs_filter_roles_checkboxes = $('.jobfilters1 input[name=jobs_filter_roles_checkboxes]:checked').map(function(){
                        return $(this).val();
                      }).get();
    var jobs_filter_teams_checkboxes = $('.jobfilters1 input[name=jobs_filter_teams_checkboxes]:checked').map(function(){
                        return $(this).val();
                      }).get();
    var jobs_filter_games_checkboxes = $('.jobfilters1 input[name=jobs_filter_games_checkboxes]:checked').map(function(){
                        return $(this).val();
                      }).get();
    var jobs_filter_locations_checkboxes = $('.jobfilters1 input[name=jobs_filter_locations_checkboxes]:checked').map(function(){
                        return $(this).val();
                      }).get();
    var jobs_filter_titles_checkboxes = $('.jobfilters1 input[name=jobs_filter_titles_checkboxes]:checked').map(function(){
                        return $(this).val();
                      }).get();
		var all_deps_jobs_post_value = jQuery( "input[name='all_deps_jobs_post_value']" ).val();

    var all_roles_checkboxes = $('.jobfilters1 .roles input[type="checkbox"]').length;
    var all_teams_checkboxes = $('.jobfilters1 .teams input[type="checkbox"]').length;
    var all_games_checkboxes = $('.jobfilters1 .games input[type="checkbox"]').length;
    var all_locations_checkboxes = $('.jobfilters1 .locations input[type="checkbox"]').length;
    var all_types_checkboxes = $('.jobfilters1 .types input[type="checkbox"]').length;
		// We can also pass the url value separately from ajaxurl for front end AJAX implementations
		var data = {
  		'action': 'pocketgems_apply_job_filter',
  		'all_deps_jobs_post_value': all_deps_jobs_post_value,
      'jobs_filter_roles_checkboxes': jobs_filter_roles_checkboxes,
      'jobs_filter_teams_checkboxes': jobs_filter_teams_checkboxes,
      'jobs_filter_games_checkboxes': jobs_filter_games_checkboxes,
      'jobs_filter_locations_checkboxes': jobs_filter_locations_checkboxes,
      'jobs_filter_titles_checkboxes': jobs_filter_titles_checkboxes,
  	};
    var jobs_filters_default_text = ['All Roles', 'All Teams', 'All Games', 'All Locations', 'All Types'];
    pocketgems_update_job_filter_text (jobs_filter_roles_checkboxes, '.jobfilters1 .roles', jobs_filters_default_text[0], all_roles_checkboxes);
    pocketgems_update_job_filter_text (jobs_filter_teams_checkboxes, '.jobfilters1 .teams', jobs_filters_default_text[1], all_teams_checkboxes);
    pocketgems_update_job_filter_text (jobs_filter_games_checkboxes, '.jobfilters1 .games', jobs_filters_default_text[2], all_games_checkboxes);
    pocketgems_update_job_filter_text (jobs_filter_locations_checkboxes, '.jobfilters1 .locations', jobs_filters_default_text[3], all_locations_checkboxes);
    pocketgems_update_job_filter_text (jobs_filter_titles_checkboxes, '.jobfilters1 .types', jobs_filters_default_text[4], all_types_checkboxes);

    jQuery.post(ajax_object.ajax_url, data, function(response) {
            if(response=='no-filter-applied'){
                jQuery('.original-results').show();
                jQuery('.job-table-msg').remove();
                jQuery('.job-table-listing-result').remove();
            }
            else {
                jQuery('.original-results').hide();
                jQuery('.job-table-msg').remove();
                jQuery('.job-table-listing-result').remove();
                jQuery('.job-table-listing').append(response);
            }
  	});
  }
  
  jQuery(".clearFilters").click(function() {    
    var job_filter = $(this).parent().find("input").prop('checked', false);
    pocketgems_apply_job_filter();
  });

  jQuery(".jobfilters1 input").change(function() {
    pocketgems_apply_job_filter();
  });
});
