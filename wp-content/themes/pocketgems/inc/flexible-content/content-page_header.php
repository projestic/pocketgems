<?php 
    $page_header = get_sub_field('title') ?: get_the_title();
?>

<?php if($page_header): ?>
    <div class="page-header text-center" id="<?php the_sub_field('anchor'); ?>">
    	<div class="container">
    		<h1 class="section-title"><?php echo $page_header; ?></h1>
    	</div>
    </div>
<?php endif; ?>

