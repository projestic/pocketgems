<?php 
    $content_boxes_section_title = get_sub_field('section_title');
    $content_boxes = get_sub_field('content_boxes');
?>

<?php if($content_boxes): ?>
    <section class="clearfix <?php the_sub_field('custom_class'); ?>" id="<?php the_sub_field('anchor'); ?>">
        
        <?php if($content_boxes_section_title): ?>
            <h2 class="section-title text-center"><?php echo $content_boxes_section_title; ?></h2>
        <?php endif; ?>

        <div class="container container-<?php the_sub_field('container'); ?> container-clear-offset">
            <?php 
                while ( have_rows('content_boxes') ) : the_row();
                    $content_box_image = get_sub_field('image');
                    $content_box_content = get_sub_field('content');
                    $content_box_reverse_columns = get_sub_field('reverse_columns');
            ?>

                <div class="content-row <?php echo get_sub_field('reverse_columns') ? 'content-row-reverse' : ''; ?>">
                    <div class="content-box">
                        <div class="image">
                            <img src="<?php echo $content_box_image['url']; ?>" alt="<?php echo $content_box_image['alt']; ?>">
                        </div>
                    </div>
                    <div class="content-box">
                        <div class="text-content">
                            <div class="text-holder">
                                <?php echo $content_box_content; ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endwhile; ?>
        </div>

    </section>
<?php endif; ?>
