<?php 
    $jumbotron_button_link = get_sub_field('button_link');
    $jumbotron_button_text = get_sub_field('button_text');
    $jumbotron_description = get_sub_field('description');
    $jumbotron_image = get_sub_field('image');
    $jumbotron_title = get_sub_field('title');
    $jumbotron_columns = get_sub_field('columns');
?>

<?php if($jumbotron_title || $jumbotron_description || ($jumbotron_button_link && $jumbotron_button_link) || $jumbotron_image || $jumbotron_columns): ?>
<section class="jumbotron-section <?php the_sub_field('custom_class'); ?>"  id="<?php the_sub_field('anchor'); ?>">
    <div class="container">

        <?php if($jumbotron_title || $jumbotron_description || ($jumbotron_button_link && $jumbotron_button_link)): ?>

            <div class="section-head section-head-bg text-center">

                <?php if($jumbotron_title): ?>
                    <h2 class="section-title"><?php echo $jumbotron_title; ?></h2>
                <?php endif; ?>

                <?php if($jumbotron_description): ?>
                    <div class="section-description"><?php echo $jumbotron_description; ?></div>
                <?php endif; ?>

                <?php if($jumbotron_button_link && $jumbotron_button_text): ?>
                    <a href="<?php echo $jumbotron_button_link; ?>" class="btn btn-primary"><?php echo $jumbotron_button_text; ?></a>
                <?php endif; ?>
                
            </div>

        <?php endif; ?>

        <?php if($jumbotron_image): ?>
            <img src="<?php echo $jumbotron_image['url']; ?>" alt="<?php echo $jumbotron_image['alt']; ?>">
        <?php endif; ?>

        <?php if($jumbotron_columns): ?>

            <div class="columns">

                <?php 
                    while ( have_rows('columns') ) : the_row();
                        $jumbotron_column_title = get_sub_field('title');
                        $jumbotron_column_description = get_sub_field('description');
                        $jumbotron_column_image = get_sub_field('image');
                        $jumbotron_column_title = get_sub_field('title');
                        $jumbotron_column_main_content = get_sub_field('main_content');
                        $jumbotron_column_expander_content = get_sub_field('expander_content');
                ?>
                    <div class="column">
                        <div class="head bg-cover" <?php echo $jumbotron_column_image['url'] ? 'style="background-image: url('.$jumbotron_column_image['url'].');"' : '' ?>>
                            <?php if($jumbotron_column_title): ?>
                                <h3 class="title"><?php echo $jumbotron_column_title; ?></h3>
                            <?php endif; ?>
                        </div>
                        <div class="content">

                            <?php if($jumbotron_column_main_content): ?>
                                <div class="main">
                                    <?php echo $jumbotron_column_main_content; ?>
                                </div>
                            <?php endif; ?>


                            <?php if($jumbotron_column_expander_content): ?>
                                <div class="expander" style="display: none;">
                                    <?php echo $jumbotron_column_expander_content; ?>
                                </div>
                            <?php endif; ?>

                            <?php if($jumbotron_column_expander_content): ?>
                                <a href="#" class="read-more read-more-toggle">Read more</a>
                            <?php endif; ?>

                        </div>
                    </div>

                <?php endwhile; ?>

            </div>

        <?php endif; ?>

    </div>
</section>
<?php endif; ?>
