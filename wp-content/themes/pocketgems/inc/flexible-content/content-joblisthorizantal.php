<?php
$jobs = getJobs(get_sub_field('board'));
$jobs_section_title = get_sub_field('section_title');

$all_deps_jobs = $jobs["result"]["departments"];
$teams = array();
$roles = array();
$games = array();
$location = 'San Francisco';
$type = 'Full-time';
$all_deps_jobs_post_value_array = array();
$index = 0;
if (count($all_deps_jobs)) {
  foreach ($all_deps_jobs as $dep => $all_dep_jobs) {
      if(!in_array(trim($dep), $teams)){
          $teams [] = trim($dep);
      }

      foreach ($all_dep_jobs as $job_key =>$job) {
          if(!in_array(trim($job['title']), $roles)){
              $roles[] = trim($job['title']);
          }
          if(!in_array(trim($job['location']['name']), $games)){
              $games[] = trim($job['location']['name']);
          }
          $all_deps_jobs_post_value_array[$index]['teams'] = trim($dep);
          $all_deps_jobs_post_value_array[$index]['absolute_url'] = trim($job['absolute_url']);
          $all_deps_jobs_post_value_array[$index]['roles'] = trim($job['title']);
          $all_deps_jobs_post_value_array[$index]['games'] = trim($job['location']['name']);
          $all_deps_jobs_post_value_array[$index]['locations'] = $location;
          $all_deps_jobs_post_value_array[$index]['titles'] = $type;
          $index++;
      }
  }
}

usort($roles, 'pocketgems_sort_alphabetic');
usort($games, 'pocketgems_sort_alphabetic');
usort($teams, 'pocketgems_sort_alphabetic');

$all_deps_jobs_post_value = base64_encode(serialize($all_deps_jobs_post_value_array));

?>

<div class="text-content" id="<?php the_sub_field('anchor'); ?>">
    <div class="container container-lg searchFilters" >

        <div class="text-center searchFilterButtonWrapper">
            <a href="#" id="btnSeaerchFilters">Open Search Filters</a>
        </div>

        <div class="jobfilters1 container">
            <div class="box roles">
                <a href="#" class="showFilter"><span class="title">Roles</span> <span class="dropdown-arrow"></span></a>
                <p class="text-muted">All Roles</p>
                <div class="inner" id="roles">
                    <?php foreach ($roles as $role){
                            $role_html_id = strtolower(str_replace(' ', '-', $role));?>
                        <div class="input-group checkbox">
                            <input id="<?php echo $role_html_id; ?>" name="jobs_filter_roles_checkboxes" class="jobs_filter_checkboxes option" value="<?php echo $role; ?>" type="checkbox">
                            <label class="label" for="<?php echo $role_html_id; ?>"><?php echo $role; ?></label>
                        </div>
                    <?php } ?>
                </div>
                <p class="clearFilters"><a href="javascript:;">Clear</a></p>
            </div>

            <div class="box teams">
                <a href="#" class="showFilter"><span class="title">Teams</span> <span class="dropdown-arrow"></span></a>
                <p class="text-muted">All Teams</p>
                <div class="inner" id="Teams">
                    <?php foreach ($teams as $team){
                            $team_html_id = strtolower(str_replace(' ', '-', $team));?>
                        <div class="input-group checkbox">
                            <input id="<?php echo $team_html_id; ?>" name="jobs_filter_teams_checkboxes" class="jobs_filter_checkboxes option" value="<?php echo $team; ?>" type="checkbox">
                            <label class="label" for="<?php echo $team_html_id; ?>"><?php echo $team; ?></label>
                        </div>
                    <?php } ?>
                </div>
                <p class="clearFilters"><a href="javascript:;">Clear</a></p>
            </div>

            <div class="box games">
                <a href="#" class="showFilter"><span class="title">Studios</span> <span class="dropdown-arrow"></span></a>
                <p class="text-muted">All Studios</p>
                <div class="inner" id="Games">
                    <?php foreach ($games as $game){
                            $game_html_id = strtolower(str_replace(' ', '-', $game));?>
                        <div class="input-group checkbox">
                            <input id="<?php echo $game_html_id; ?>" name="jobs_filter_games_checkboxes" class="jobs_filter_checkboxes option" value="<?php echo $game; ?>" type="checkbox">
                            <label class="label" for="<?php echo $game_html_id; ?>"><?php echo $game; ?></label>
                        </div>
                    <?php } ?>
                </div>
                <p class="clearFilters"><a href="javascript:;">Clear</a></p>
            </div>

            <div class="box locations">
                <a href="#" class="showFilter"><span class="title">Locations</span> <span class="dropdown-arrow"></span></a>
                <p class="text-muted">All Locations</p>
                <div class="inner" id="Locations">
                    <div class="input-group checkbox">
                        <?php $location_html_id = strtolower(str_replace(' ', '-', $location)); ?>
                        <input id="<?php echo $location_html_id; ?>" name="jobs_filter_locations_checkboxes" class="jobs_filter_checkboxes option" value="<?php echo $location; ?>" type="checkbox" checked>
                        <label class="label" for="<?php echo $location_html_id; ?>"><?php echo $location; ?></label>
                    </div>
                </div>
                <p class="clearFilters"><a href="javascript:;">Clear</a></p>
            </div>

            <div class="box types">
                <a href="#" class="showFilter"><span class="title">Types</span> <span class="dropdown-arrow"></span></a>
                <p class="text-muted">All Types</p>
                <div class="inner" id="type">
                    <div class="input-group checkbox">
                        <?php $type_html_id = strtolower(str_replace(' ', '-', $type)); ?>
                        <input id="<?php echo $type_html_id; ?>" name="jobs_filter_titles_checkboxes" class="jobs_filter_checkboxes option" value="<?php echo $type; ?>" type="checkbox" checked>
                        <label class="label" for="<?php echo $type_html_id; ?>"><?php echo $type; ?></label>
                    </div>
                </div>
                <p class="clearFilters"><a href="javascript:;">Clear</a></p>
            </div>

        </div>
    </div>
</div>

<?php
if (!empty($jobs['result'])):?>
    <div class="container container-lg job-table-listing">
        <div class="original-results">
    <?php
    $departments = $jobs['result']['departments'];

    if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'team'){
      if($_GET['sort'] == 'asc'){
        ksort($departments);
      }
      elseif($_GET['sort'] == 'desc'){
        krsort($departments);
      }

    }

    function table_sort_jobs($a,$b){
      if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'game'){
        if($_GET['sort'] == 'asc')
        return ($a['location']['name'] > $b['location']['name']);
        else if($_GET['sort'] == 'desc')
        return ($a['location']['name'] < $b['location']['name']);
      }
      else if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'title'){
        if($_GET['sort'] == 'asc'){
          return ($a['title'] > $b['title']);
        }
        elseif($_GET['sort'] == 'desc'){
          return ($a['title'] < $b['title']);
        }
      }
    }
    foreach ($departments as $department => $dItems) {
      usort($dItems, "table_sort_jobs");
      ?>
            <div class="padding-container">
                <h3 class="black"><?php echo $department ?></h3>
                <div class="jobs-table-wrapper">
                  <table width="100%" id="<?php echo $department ?>">
                      <thead>
                      <tr class="border_bottom gray">
                        <?php

                        if (!isset($_GET['orderBy']) && !isset($_GET['orderBy'])){
                        ?>
                          <th scope="col">
                            <a href="?orderBy=title&sort=asc">
                              <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                            </a>
                          </th>
                          <th scope="col"><a href="?orderBy=team&sort=desc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a></th>
                          <!-- <th scope="col"><span class="title">Game</span> <span class="dropdown-arrow"></span></th> -->
                          <th scope="col"><a href="?orderBy=game&sort=asc"><span class="title">Studio</span> <span class="dropdown-arrow"></span></a></th>
                          <th scope="col"><span class="title">Location</span></th>
                          <th scope="col"><span class="title">Type</span></th>
                        <?php
                        }
                        else {


                        ?>
                        <?php
                        if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'title' && $_GET['sort'] == 'asc'){
                        ?>
                            <th scope="col" class="ascending">
                            <a href="?orderBy=title&sort=desc">
                              <span class="title" class="ascending">Roles</span> <span class="dropdown-arrow"></span>
                            </a>
                          </th>
                        <?php
                        } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'title' && $_GET['sort'] == 'desc') {
                        ?>
                          <th scope="col" class="descending">
                            <a href="?orderBy=title&sort=asc">
                              <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                            </a>
                          </th>
                        <?php
                        }
                         else {
                        ?>
                          <th scope="col">
                            <a href="?orderBy=title&sort=asc">
                              <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                            </a>
                          </th>
                        <?php
                        }
                        if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'team' && $_GET['sort'] == 'asc'){
                        ?>

                          <th scope="col" class="ascending">
                            <a href="?orderBy=team&sort=desc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                          </th>

                        <?php
                        } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'team' && $_GET['sort'] == 'desc') {
                        ?>

                          <th scope="col" class="descending">
                            <a href="?orderBy=team&sort=asc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                          </th>
                        <?php
                        } else {
                        ?>
                          <th scope="col">
                            <a href="?orderBy=team&sort=asc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                          </th>

                        <?php
                        }
                        if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'game' && $_GET['sort'] == 'asc'){
                        ?>
                          <th scope="col" class="ascending">
                            <a href="?orderBy=game&sort=desc">
                              <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                            </a>
                          </th>


                        <?php
                        } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'game' && $_GET['sort'] == 'desc') {
                        ?>
                          <th scope="col" class="descending">
                            <a href="?orderBy=game&sort=asc">
                              <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                            </a>
                          </th>

                        <?php
                        } else {
                        ?>
                          <th scope="col">
                            <a href="?orderBy=game&sort=asc">
                              <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                            </a>
                          </th>

                        <?php
                        }
                        ?>

                          <th scope="col">

                              <span class="title">Location</span>
                          </th>

                          <th scope="col">
                            <span class="title">Type</span>
                          </th>
                        <?php
                      }
                      ?>
                      </tr>
                      </thead>
                      <?php foreach ($dItems as $dItem) { ?>
                          <tr class="border_bottom">
                              <td  data-label="Role">
                                  <a href="<?php echo $dItem['absolute_url']; ?>"><?php echo $dItem['title'] ?></a>
                              </td>
                              <td data-label="Team"><?php echo $department ?></td>
                              <td data-label="Studio"><?php echo $dItem['location']['name']; ?></td>
                              <td data-label="Location"><?php echo $location; ?></td>
                              <td data-label="Type"><?php echo $type; ?></td>
                          </tr>
                      <?php } ?>
                  </table>
                </div>
            </div>
    <?php } ?>
        </div>
    </div>
<?php endif; ?>
<input type="hidden" name="all_deps_jobs_post_value" value="<?php echo $all_deps_jobs_post_value; ?>" />
