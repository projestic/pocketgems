<?php 
    $beliefs = get_sub_field('beliefs');
    $beliefs_section_title = get_sub_field('section_title');
    $beliefs_counter = 0;
    $num_grid = array('top right', 'bottom right', 'top left', 'bottom left');

?>

<?php if($beliefs): ?>
    <section class="beliefs" id="<?php the_sub_field('anchor'); ?>">
        <div class="container">
            
            <?php if($beliefs_section_title): ?>
                <h2 class="section-title text-center"><?php echo $beliefs_section_title; ?></h2>
            <?php endif; ?>

            <div class="row">

                <?php 
                    while ( have_rows('beliefs') ) : the_row();
                        $belief_image = get_sub_field('image');
                        $belief_text = get_sub_field('text');
                ?>
                    <div class="col-lg-3 col-sm-6">
                        <div class="ico-holder">
                            <div class="ico">
                                <?php if($belief_image): ?>
                                    <img src="<?php echo $belief_image['url']; ?>" alt="<?php echo $belief_image['alt']; ?>">
                                <?php endif; ?>
                                
                                <span class="num <?php echo $num_grid[$beliefs_counter%4]; ?>"><?php echo $beliefs_counter + 1; ?></span>
                            </div>
                        </div>

                        <?php if($belief_text): ?>
                            <div class="description"><?php echo $belief_text; ?></div>
                        <?php endif; ?>
                        
                    </div>

                <?php 
                        $beliefs_counter++;
                    endwhile; 
                ?>

            </div>
        </div>
    </section>
<?php endif; ?>
