<?php
$location = 'San Francisco';
if(isset($_POST['jobSearch']) && $_POST['jobSearch'] != ''){
    $responseError      = false;
    $board              = get_sub_field('board');
    $jobResponse        = wp_remote_get( 'https://api.greenhouse.io/v1/boards/'.$board.'/jobs?content=true');
    if (is_wp_error($jobResponse)) {

        $responseError  = true;

    } else/*if ($jobResponse['response']['code'] != 200) */{



        $allJobs            = json_decode($jobResponse['body'], true);

        if (isset($allJobs['jobs']) && !empty($allJobs['jobs'])) {
            $allJobs        = $allJobs['jobs'];
            $searchResult   = array();

            foreach ($allJobs as $key => $jobIn) {

                $isJobAdded     = false;
                $jobTitle       = $jobIn['title'];
                $offices        = $jobIn['offices'];
                $departments    = $jobIn['departments'];

                if (stripos($jobTitle, $_POST['jobSearch']) !== false) {
                    $searchResult[]     = $allJobs[$key];
                    $isJobAdded         = true;
                }

                if (!$isJobAdded) {
                    foreach ($offices as $officeIn) {
                        $officeName         = $officeIn['name'];
                        $officeLocation     = $officeIn['location'];

                        if (strpos($officeName, $_POST['jobSearch']) !== false) {
                            $searchResult[]     = $allJobs[$key];
                            $isJobAdded         = true;
                            break;
                        } elseif (strpos($officeLocation, $_POST['jobSearch']) !== false) {
                            $searchResult[]     = $allJobs[$key];
                            $isJobAdded         = true;
                            break;
                        }
                    }
                }

                if (!$isJobAdded) {
                    foreach ($departments as $departmentIn) {
                        $departmentName             = $departmentIn['name'];
                        if (strpos($departmentName, $_POST['jobSearch']) !== false) {
                            $searchResult[]         = $allJobs[$key];
                            $isJobAdded             = true;
                            break;
                        }
                    }
                }

            }
        }
    }

    ?>
    <!--after Search area start-->
    <div class="padding-container job-table-listing">
        <h3 class="black"><?php echo 'Search results' ?>
            <small class="clearPage"><a  href="<?php echo esc_url(get_permalink(893)); ?>" class="blue">Clear search results</a></small>
        </h3>
        <div class="jobs-table-wrapper">


          <table width="100%">
            <thead>
            <tr class="border_bottom gray">
              <?php

              if (!isset($_GET['orderBy']) && !isset($_GET['orderBy'])){
              ?>
                <th scope="col">
                  <a href="?orderBy=title&sort=asc">
                    <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                  </a>
                </th>
                <th scope="col"><a href="?orderBy=team&sort=desc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a></th>
                <!-- <th scope="col"><span class="title">Game</span> <span class="dropdown-arrow"></span></th> -->
                <th scope="col"><a href="?orderBy=game&sort=asc"><span class="title">Studio</span> <span class="dropdown-arrow"></span></a></th>
                <th scope="col"><span class="title">Location</span></th>
                <th scope="col"><span class="title">Type</span></th>
              <?php
              }
              else {


              ?>
              <?php
              if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'title' && $_GET['sort'] == 'asc'){
              ?>
                  <th scope="col" class="ascending">
                  <a href="?orderBy=title&sort=desc">
                    <span class="title" class="ascending">Roles</span> <span class="dropdown-arrow"></span>
                  </a>
                </th>
              <?php
              } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'title' && $_GET['sort'] == 'desc') {
              ?>
                <th scope="col" class="descending">
                  <a href="?orderBy=title&sort=asc">
                    <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                  </a>
                </th>
              <?php
              }
               else {
              ?>
                <th scope="col">
                  <a href="?orderBy=title&sort=asc">
                    <span class="title">Roles</span> <span class="dropdown-arrow"></span>
                  </a>
                </th>
              <?php
              }
              if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'team' && $_GET['sort'] == 'asc'){
              ?>

                <th scope="col" class="ascending">
                  <a href="?orderBy=team&sort=desc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                </th>

              <?php
              } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'team' && $_GET['sort'] == 'desc') {
              ?>

                <th scope="col" class="descending">
                  <a href="?orderBy=team&sort=asc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                </th>
              <?php
              } else {
              ?>
                <th scope="col">
                  <a href="?orderBy=team&sort=asc"><span class="title">Team</span> <span class="dropdown-arrow"></span></a>
                </th>

              <?php
              }
              if (isset($_GET['orderBy']) && $_GET['orderBy'] == 'game' && $_GET['sort'] == 'asc'){
              ?>
                <th scope="col" class="ascending">
                  <a href="?orderBy=game&sort=desc">
                    <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                  </a>
                </th>


              <?php
              } elseif (isset($_GET['orderBy']) && $_GET['orderBy'] == 'game' && $_GET['sort'] == 'desc') {
              ?>
                <th scope="col" class="descending">
                  <a href="?orderBy=game&sort=asc">
                    <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                  </a>
                </th>

              <?php
              } else {
              ?>
                <th scope="col">
                  <a href="?orderBy=game&sort=asc">
                    <span class="title">Studio</span> <span class="dropdown-arrow"></span></span>
                  </a>
                </th>

              <?php
              }
              ?>

                <th scope="col">

                    <span class="title">Location</span>
                </th>

                <th scope="col">
                  <span class="title">Type</span>
                </th>
              <?php
            }
            ?>
            </tr>
            </thead>
            <?php if(isset($searchResult) && !empty($searchResult)){
                foreach ($searchResult as $Item) { ?>
                    <tr class="border_bottom">
                        <td data-label="Role">
                            <a href="<?php echo $Item['absolute_url']; ?>"><?php echo $Item['title'] ?></a>
                        </td>
                        <td data-label="Team"><?php echo $Item['departments'][0]['name']; ?></td>
                        <td data-label="Studio"><?php echo $Item['location']['name']; ?></td>
                        <td data-label="Location"><?php echo $location; ?></td>
                        <td data-label="Type">Full-time</td>
                    </tr>
                <?php }
            }else{?>
                <tr class="border_bottom">
                    <td>
                        <?php
                        if($responseError){
                            echo ' API response error';
                        }else{
                            echo "No record found";
                        }
                        ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

           <?php }
            ?>
          </table>
        </div>
    </div>
    <!--after Search area end-->

<?php }else{?>
<!--before Search area start-->
<div class="container container-lg padding-container">
    <div class="before-search container">
        <p class="before-search-text centered"><span class="blue">Searching.</span> <span class="black">Hang tight.</span></p>
    </div>
</div>
<!--before Search area end-->
    <style>
        #apply-direct-main{
            display: none!important;
        }
    </style>

<?php } ?>
