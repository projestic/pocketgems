<?php 
    $testimonials = get_sub_field('testimonials');
    $testimonials_button_text = get_sub_field('button_text');
    $testimonials_button_link = get_sub_field('button_link');
?>

<?php if($testimonials): ?>
    
    <div class="container">
        <section class="testimonials" id="<?php the_sub_field('anchor'); ?>">
            <div class="row">
                <?php 
                    while ( have_rows('testimonials') ) : the_row();
                        $testimonial_image = get_sub_field('image');
                        $testimonial_name = get_sub_field('name');
                        $testimonial_post = get_sub_field('post');
                        $testimonial_quote = get_sub_field('quote');
                ?>
                    <div class="col-sm-4">
                        <div class="ico" <?php echo $testimonial_image['sizes']['thumbnail'] ? 'style="background-image: url('.$testimonial_image['sizes']['thumbnail'].');"' : '' ?>></div>

                        <?php if($testimonial_name): ?>
                            <div class="title"><?php echo $testimonial_name; ?></div>
                        <?php endif; ?>

                        <?php if($testimonial_post): ?>
                            <div class="title"><?php echo $testimonial_post; ?></div>
                        <?php endif; ?>
                        
                        <?php if($testimonial_quote): ?>
                            <q><?php echo $testimonial_quote; ?></q>
                        <?php endif; ?>
                    </div>

                <?php endwhile; ?>

            </div>
            <?php 
                $testimonials_button_text = get_sub_field('button_text');
                $testimonials_button_link = get_sub_field('button_link');
            ?>

            <?php if($testimonials_button_text && $testimonials_button_link): ?>
                <div class="btn-holder">
                    <a href="<?php echo $testimonials_button_link; ?>" class="btn btn-primary"><?php echo $testimonials_button_text; ?></a>
                </div>
            <?php endif; ?>
        </section>
    </div>

<?php endif; ?>