
<?php 
    $jobs = getJobs(get_sub_field('board'));
    $jobs_section_title = get_sub_field('section_title');
?>

<?php if(!empty($jobs['result'])): ?>
    <section class="open-jobs" id="<?php the_sub_field('anchor'); ?>">
        <div class="container container-md" style="padding-top:2%">

        <?php if($jobs_section_title): ?>
            <h2 class="section-title text-center"><?php echo $jobs_section_title; ?></h2>
        <?php endif; ?>

            <div class="tabset show-all">

                <div class="nav-col">
                    <ul class="nav tab-menu" role="tablist">
                        <li role="jobs" class="active"><a href="#tabAll">Show all</a></li>

                        <?php 
                            foreach ($jobs['result']['departments'] as $key => $value): 
                                $slug = sanitize_title_with_dashes($key);
                        ?>
                            <li role="jobs"><a href="#<?php echo $slug; ?>" aria-controls="<?php echo $slug; ?>" role="tab" data-toggle="tab"><?php echo $key; ?></a></li>
                        <?php 
                            endforeach;
                        ?>

                        <li class="sep"></li>

                        <?php 
                            foreach ($jobs['result']['offices'] as $key => $value): 
                                $slug = sanitize_title_with_dashes($key);
                        ?>
                            <li role="jobs"><a href="#<?php echo $slug; ?>" aria-controls="<?php echo $slug; ?>" role="tab" data-toggle="tab"><?php echo $key; ?></a></li>
                        <?php 
                            endforeach;
                        ?>

                    </ul>
                </div>

                <div class="content-col">
                    <div class="tab-content">

                        <?php 
                            $all_jobs = $jobs['result']['departments'] + $jobs['result']['offices'];
                            foreach ($all_jobs as $key => $department): 
                                $slug = sanitize_title_with_dashes($key);
                        ?>

                            <div role="tabpanel" class="tab-pane fade in active" id="<?php echo $slug; ?>">
                                <h3 class="tab-title"><?php echo $key; ?></h3>

                                <?php foreach ($department as $key => $job): ?>
                                    <a href="<?php echo $job['absolute_url']; ?>" class="job-promo">
                                        <h4 class="title"><?php echo $job['title']; ?></h4>
                                        <div class="description"><?php echo $job['location']['name']; ?></div>
                                    </a>
                                <?php endforeach; ?>
                            </div>

                        <?php endforeach; ?>

                    </div>
                </div>

            </div>
        </div>
    </section>
<?php endif; ?>
