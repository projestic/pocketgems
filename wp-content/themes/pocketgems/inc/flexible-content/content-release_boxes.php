<?php 
    $release_boxes = get_sub_field('release_boxes');
?>

<?php if($release_boxes): ?>

    <section class="text-center releases"  id="<?php the_sub_field('anchor'); ?>">
        <div class="container-clear-offset">
            <div class="content-row">

                <?php 
                    while ( have_rows('release_boxes') ) : the_row();
                        $release_box_bg = get_sub_field('background');
                        $release_box_image = get_sub_field('logo');
                        $release_box_read_more_link = get_sub_field('read_more_link');
                        $release_box_download_link = get_sub_field('download_link');
                    ?>

                    <div class="content-box content-box-align-top square-box bg-cover" <?php echo $release_box_bg['url'] ? 'style="background-image: url('.$release_box_bg['url'].');"' : '' ?>>
                        <div class="text-content">
                            <?php if($release_box_image): ?>
                                <div class="release-logo">
                                    <img class="img-responsive" src="<?php echo $release_box_image['url']; ?>" alt="<?php echo $release_box_image['alt']; ?>">
                                </div>
                            <?php endif; ?>

                            <div class="btns-row">
                                <?php if($release_box_read_more_link): ?>
                                    <a href="<?php echo $release_box_read_more_link; ?>" class="btn btn-primary">Learn more</a>
                                <?php endif; ?>

                                <?php if($release_box_download_link): ?>
                                    <a href="<?php echo $release_box_download_link; ?>" class="btn btn-primary">Download now</a>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>

                <?php endwhile; ?>
            </div>
        </div>
    </section>

<?php endif; ?>

