<?php 
    $content_news = get_sub_field('news');
    $content_news_title = get_sub_field('section_title');
?>

<?php if($content_news): ?>
    <div class="container container-lg container-clear-offset"  id="<?php the_sub_field('anchor'); ?>">
        
        <?php if($content_news_title): ?>
            <h2 class="section-title text-center"><?php echo $content_news_title; ?></h2>
        <?php endif; ?>

        <div class="content-row">

            <?php 
                while ( have_rows('news') ) : the_row();
                    $news_logo = get_sub_field('logo');
                    $news_date = get_sub_field('date') ?: date('F j, Y');
                    $news_excerpt = get_sub_field('excerpt');
                    $news_link = get_sub_field('link');
            ?>

                <div class="content-box news-box square-box">
                    <div class="text-content post-content">
                        <?php if($news_logo): ?>
                            <div class="logo">
                                <img src="<?php echo $news_logo['url']; ?>" alt="<?php echo $news_logo['alt']; ?>">
                            </div>
                        <?php endif; ?>
                        
                        <?php if($news_date): ?>
                            <time pubdate="<?php echo $news_date; ?>"><?php echo $news_date; ?></time>
                        <?php endif; ?>
                        
                        <?php if($news_excerpt): ?>
                            <h3 class="title"><?php echo $news_excerpt; ?></h3>
                        <?php endif; ?>

                        <?php if($news_link): ?>
                            <a class="btn transparent btn-primary" href="<?php echo $news_link; ?>">Read more</a>
                        <?php endif; ?>
                        
                    </div>
                </div>

            <?php endwhile; ?>

        </div>
    </div>
<?php endif; ?>
