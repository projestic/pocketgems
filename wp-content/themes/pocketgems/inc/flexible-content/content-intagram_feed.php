<?php
    $intagram_feed = get_sub_field('description');
    $content_title = get_sub_field('title');
?>

<?php if($intagram_feed): ?>
    <div class="instagram container container-lg container-clear-offset"  id="<?php the_sub_field('anchor'); ?>">
        <?php if($content_title): ?>
            <h2 class="section-title text-center"><?php echo $content_title; ?></h2>
        <?php endif; ?>
        <div class="content-row">
            <?php echo $intagram_feed; ?>
        </div>
    </div>
<?php endif; ?>
