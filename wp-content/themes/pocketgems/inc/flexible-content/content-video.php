<?php 
    $video_section_title = get_sub_field('section_title');
    $videos = get_sub_field('videos');
    $testimonials_button_link = get_sub_field('button_link');
?>

<?php if($videos): ?>

    <section class="video-section" id="<?php the_sub_field('anchor'); ?>">

        <?php if($video_section_title): ?>
            <div class="container">
                <h2 class="section-title text-center"><?php echo $video_section_title; ?></h2>
            </div>
        <?php endif; ?>

        <div class="owl-carousel video-carousel" data-slide-count="<?php echo count($videos); ?>">
            <?php 
                while ( have_rows('videos') ) : the_row();
                    $video_url = get_sub_field('url');
                    $video_title = get_sub_field('title');
            ?>
                <div class="item-video" data-video-preview="<?php the_sub_field('preview'); ?>">
                    <a class="owl-video" href="<?php echo $video_url; ?>"></a>
                    <?php if($video_title): ?>
                        <div class="owl-title"><?php  the_sub_field('title'); ?></div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>

    </section>

<?php endif; ?>
