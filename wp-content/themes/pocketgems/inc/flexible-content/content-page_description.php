<?php
    $page_description = get_sub_field('description');
?>

<?php if($page_description): ?>
    <div class="page-description"  id="<?php the_sub_field('anchor'); ?>">
        <div class="container container-xs">
            <div class="text-big"><?php echo $page_description; ?></div>
        </div>
    </div>
<?php endif; ?>
