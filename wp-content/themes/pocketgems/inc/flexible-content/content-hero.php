<?php 
    $hero_screens = get_sub_field('screens');
    $screens_count = count($hero_screens);
?>

<?php if($screens_count): ?>
    <div class="hero" id="<?php the_sub_field('anchor'); ?>">
        
    
        <?php if($screens_count > 1): ?>
            <div class="hero-slider owl-carousel">
        <?php endif ?>

            <?php 
                while ( have_rows('screens') ) : the_row(); 
                    $screen_button_link = get_sub_field('button_link');
                    $screen_button_text = get_sub_field('button_text');
                    $screen_description = get_sub_field('description');
                    $screen_image = get_sub_field('image');
                    $screen_style = get_sub_field('style');
                    $screen_title = get_sub_field('title');
                    $screen_video_mp4 = get_sub_field('video_mp4');
                    $screen_video_webm = get_sub_field('video_webm');
            ?>
                    <div class="hero-section hero-section-<?php echo $screen_style; ?>">
                        <div class="cell">

                            <?php if(empty($screen_video_mp4) || empty($screen_video_webm)): ?>
                                <div class="bg" style="background-image: url(<?php echo $screen_image['url']; ?>)"></div>

                                <img class="img-placeholder" src="<?php echo DIR.'/assets/images/empty-hero-desctop.png' ?>">
                            <?php endif; ?>

                            

                            <?php if($screen_video_mp4 || $screen_video_webm): ?>
                                <div class="videoplayer">
                                    <video <?php echo $screen_image ? 'poster="'.$screen_image['url'].'" ' : ' '; ?> autoplay muted loop>
                                        <?php if($screen_video_webm): ?>
                                            <source src="<?php echo $screen_video_webm['url']; ?>" type="video/webm">
                                        <?php endif; ?>

                                        <?php if($screen_video_mp4): ?>
                                            <source src="<?php echo $screen_video_mp4['url']; ?>" type="video/mp4">
                                        <?php endif; ?>
                                    </video>
                                </div>
                            <?php endif; ?>

                            <?php if($screen_button_link || $screen_button_text || $screen_description || $screen_title): ?>

                                <div class="hero-content">

                                    <div class="container">

                                        <?php if($screen_title): ?>
                                            <h2 class="section-title"><?php echo $screen_title; ?></h2>
                                        <?php endif; ?>

                                        <?php if($screen_description): ?>
                                            <div class="section-description"><?php echo $screen_description; ?></div>
                                        <?php endif; ?>

                                        <?php if($screen_button_link && $screen_button_text): ?>
                                            <a class="btn btn-primary" href="<?php echo $screen_button_link; ?>"><?php echo $screen_button_text; ?></a>
                                        <?php endif; ?>

                                    </div>
                                </div>


                            <?php endif; ?>
                        </div>
                    </div>

            <?php endwhile; ?>

        <?php if($screens_count > 1): ?>
            </div>
        <?php endif ?>

    </div>

<?php endif; ?>
