<?php 
    $product_description = get_sub_field('description') ?: get_the_content();
    $product_download_link = get_sub_field('download_link');
    $product_site_link = get_sub_field('site_link');
    $product_support_link = get_sub_field('support_link');
    $product_title = get_sub_field('title') ?: get_the_title();
?>

<section class="game-promo text-center"  id="<?php the_sub_field('anchor'); ?>">
    <div class="container container-xs">

        <?php if($product_download_link || $product_site_link): ?>
            <div class="btns-row">
                <?php if($product_download_link): ?>
                    <a href="<?php echo $product_download_link; ?>" class="btn btn-primary">Download Now</a>
                <?php endif; ?>
                <?php if($product_site_link): ?>
                    <a href="<?php echo $product_site_link; ?>" class="btn btn-primary">Official Site</a>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if($product_title): ?>
            <h2 class="section-title"><?php echo $product_title; ?></h2>
        <?php endif; ?>

        <?php if($product_description): ?>
            <div class="section-description"><?php echo $product_description; ?></div>
        <?php endif; ?>

        <?php if($product_support_link): ?>
            <a href="<?php echo $product_support_link; ?>" class="btn btn-primary transparent" target="_blank">Support</a>
        <?php endif; ?>

    </div>
</section>