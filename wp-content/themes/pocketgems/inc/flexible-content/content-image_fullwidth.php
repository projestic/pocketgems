<?php 
	$image_fullwidth = get_sub_field('image');
?>

<?php if($image_fullwidth): ?>
<section class="image-fullwidth"  id="<?php the_sub_field('anchor'); ?>">
	<img src="<?php echo $image_fullwidth['url']; ?>" alt="<?php echo $image_fullwidth['alt']; ?>">
</section>
<?php endif; ?>

