<?php
    $team_with_hire_date = get_posts(array(
        'numberposts' => -1,
        'meta_key'		=> 'hire_date',
      	'orderby'			=> 'meta_value',
      	'order'				=> 'ASC',
        'post_type'   => 'team'
    ));
    $team_id_with_hire_date = pocketgems_get_team_members_id($team_with_hire_date);

    $team_without_hire_date = get_posts(array(
        'numberposts' => -1,
        'order'				=> 'ASC',
        'exclude'     => $team_id_with_hire_date,
        'post_type'   => 'team'
    ));

    $team = array_merge($team_with_hire_date, $team_without_hire_date);
    $people_section_title = get_sub_field('section_title');
?>

<?php if(!empty($team)): ?>
    <div class="container container-lg"  id="<?php the_sub_field('anchor'); ?>">

        <?php if($people_section_title): ?>
            <h2 class="section-title text-center"><?php echo $people_section_title; ?></h2>
        <?php endif; ?>

        <div class="team-grid">

            <?php
                foreach ($team as $team_member):
                    $team_member_image = wp_get_attachment_image_src(get_post_thumbnail_id($team_member->ID), 'post-thumbnail');
                    $team_member_name = get_field('display_name', $team_member->ID) ?: $team_member->post_title;
                    $team_member_position = get_field('position', $team_member->ID);
                    $team_member_hire = get_field('hire_date', $team_member->ID);
                    $team_member_description = get_field('description', $team_member->ID);
            ?>

                    <figure class="item" template="">
                        <div class="image" <?php echo $team_member_image[0] ? 'style="background-image: url('.$team_member_image[0].');"' : ''; ?>></div>
                        <div class="user-popup">
                                <div class="arrow"></div>
                                <img class="close-icon" src="<?php echo DIR; ?>/assets/images/x@2x.png" alt="Close icon">

                                <?php echo $team_member_description; ?>
                                <?php echo $team_member_hire; ?>
                            </div>
                        <figcaption>
                            <strong><?php echo $team_member_name; ?></strong> <?php echo $team_member_position; ?>
                        </figcaption>
                    </figure>


            <?php
                endforeach;
            ?>
        </div>
    </div>

<?php endif; ?>
