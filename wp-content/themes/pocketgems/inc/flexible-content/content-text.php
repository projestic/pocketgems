<?php 
    $text_content = get_sub_field('content');
?>

<?php if($text_content): ?>
    <div class="text-content" id="<?php the_sub_field('anchor'); ?>">
        <div class="container container-<?php the_sub_field('container'); ?>">
            <?php echo $text_content; ?>
        </div>
    </div>
<?php endif; ?>

