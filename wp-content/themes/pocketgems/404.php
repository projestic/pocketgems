<?php get_header(); ?>

<div class="page-header text-center">
	<div class="container">
		<h1>Page not found</h1>
		<p>Please check the URL for proper spelling and capitalization.
		<a class="btn btn-primary btn-home-page" style="display: block; width: 117.5px; margin: 5% auto 0 auto;" href="/">home page</a>
	</div>
</div>

<?php get_footer(); ?>
