<?php get_header(); ?>

<?php 
	$has_hero = false;

	while (have_posts()) : the_post();

		if( have_rows('flexible_content') ): 
	        while ( have_rows('flexible_content') ) : the_row();
	            $has_hero = true;
	        endwhile;
	    endif; 
	
		if(!$has_hero): 
	?>
		<div class="page-header text-center">
			<div class="container">
				<h1 class="section-title"><?php the_title(); ?></h1>
			</div>
		</div>
	<?php 
		endif; 

		include 'inc/content.php'; 
	
	endwhile; 
?>

<?php get_footer(); ?>
