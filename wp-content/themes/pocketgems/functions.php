<?php
# Enqueue Custom Scripts
define('DIR', get_template_directory_uri());

add_action( 'wp_enqueue_scripts', 'pocketgems_wp_enqueue_scripts' );
$all_jobs_data = array();
function pocketgems_wp_enqueue_scripts() {

    acf_add_options_page();

    # enqueue custom styles
    wp_enqueue_style( 'app.css', DIR . '/assets/css/app.css', false, filemtime(get_stylesheet_directory().'/assets/css/app.css') );

    // wp_enqueue_style( 'bootstrap_css', esc_url_raw( 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' ));

    # enqueue custom scripts
    wp_enqueue_script('app.js', DIR . '/assets/js/app.js', false,  filemtime(get_stylesheet_directory().'/assets/js/app.js') );

    wp_enqueue_script('bootstrap_js', esc_url_raw( '"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' ) );

    wp_enqueue_script( 'ajax-script', DIR . '/assets/js/pocketgems_apply_job_filter.js');

  	// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
  	wp_localize_script( 'ajax-script', 'ajax_object',
              array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    // add_action( 'plugins_loaded', 'add_my_options_page' );

}

if ( ! function_exists( 'pocketgems_setup' ) ) :


    show_admin_bar(false);

    function pocketgems_setup() {

        register_nav_menus( array(
            'primary_navigation' => __( 'Primary Menu', 'pocketgems' )
        ) );

        register_nav_menus( array(
            'secondary_main_menu' => __( 'Secondary Main Menu', 'pocketgems' )
        ) );

        register_nav_menus( array(
            'primary_footer_navigation' => __( 'Primary Footer Menu', 'pocketgems' )
        ) );

        register_nav_menus( array(
            'secondary_footer_navigation' => __( 'Secondary Footer Menu', 'pocketgems' )
        ) );
    }

    acf_add_options_page();

    // add_action( 'plugins_loaded', 'add_my_options_page' );

    add_theme_support( 'post-thumbnails');

endif;

function reset_editor()
{
     global $_wp_post_type_features;

     $post_type="page";
     $feature = "editor";
     if ( !isset($_wp_post_type_features[$post_type]) ) {

     }
     elseif ( isset($_wp_post_type_features[$post_type][$feature]) )
     unset($_wp_post_type_features[$post_type][$feature]);
}

add_action("init","reset_editor");

add_action( 'after_setup_theme', 'pocketgems_setup' );

function getJobs($board = 'pocketgems') {
    return  handleStartProcessResponse(wp_remote_get( 'https://api.greenhouse.io/v1/boards/'.$board.'/offices'));
}


/**
* @param array|WP_Error $response
* @return array
*/
function handleStartProcessResponse($response) {

    $result = [
        'result' => 1,
        'message' => '',
        'data' => ''
     ];

     if (is_wp_error($response)) {

         $result['result'] = 0;
         $error = array_shift($response->errors);
         $result['message'] = array_shift($error);

     } elseif ($response['response']['code'] != 200) {

         $body = json_decode($response['body'], true);
         $result['result'] = 0;
         $result['message'] = sprintf('Application responded with code %s and message %s. %s',
             $response['response']['code'],
             $response['response']['message'],
             print_r($body, true)
         );

     } else {

         $arr = array();

         $arr_offices = array();
         $officeList = array();

         $body = json_decode($response['body'], true);

         $offices = $body['offices'];

         foreach ($offices as $key => $office) {

             foreach ($office['departments'] as $key => $department){

                 foreach ($department['jobs'] as $key => $job) {

                        $arr[$department['name']][] = $job;

                        if($office['name'] == 'Core') {
                            $arr_offices['Core Studio'][] = $job;
                        }

                        if($office['name'] == 'Episode') {
                            $arr_offices['Episode'][] = $job;
                        }

                        if($office['name'] == 'War Dragons') {
                            $arr_offices['War Dragons'][] = $job;
                        }
                    }
                }
             unset($office['departments']);
             $officeList[] = $office;

            }



            ksort($arr);

            ksort($arr_offices);


            $result['result'] = array(
                'departments' => $arr,
                'offices'     => $arr_offices,
                'officeList'=>$officeList,
            );
        }

    return $result;
}

function post_team() {

    register_post_type( 'team',
        array( 'labels' => array(
            'name' => __( 'Team', 'pocketgems' ), /* This is the Title of the Group */
            'singular_name' => __( 'Team', 'pocketgems' ), /* This is the individual type */
            'all_items' => __( 'All Team Members', 'pocketgems' ), /* the all items menu item */
            'add_new' => __( 'Add New', 'tiny_elephant' ), /* The add new menu item */
            'add_new_item' => __( 'Add New team Member', 'pocketgems' ), /* Add New Display Title */
            'edit' => __( 'Edit', 'pocketgems' ), /* Edit Dialog */
            'edit_item' => __( 'Edit Team Member', 'pocketgems' ), /* Edit Display Title */
            'new_item' => __( 'New Team Member', 'pocketgems' ), /* New Display Title */
            'view_item' => __( 'View Team Member', 'pocketgems' ), /* View Display Title */
            'search_items' => __( 'Search Team Member', 'pocketgems' ), /* Search Service Title */
            'not_found' =>  __( 'Nothing found in the Database.', 'pocketgems' ), /* This displays if there are no entries yet */
            'not_found_in_trash' => __( 'Nothing found in Trash', 'pocketgems' ), /* This displays if there is nothing in the trash */
            'parent_item_colon' => '',
            ''
            ), /* end of arrays */
            'description' => __( 'This is the Service type', 'pocketgems' ), /* Service Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-admin-users', /* the icon for the Service type menu */
            'rewrite'   => array( 'slug' => 'team', 'with_front' => false ), /* you can specify its url slug */
            'has_archive' => false, /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            'taxonomies' => array('service_category'),
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array('title', 'thumbnail', 'page-attributes', 'post-formats')
        )
    );
}

add_action( 'init', 'post_team');

add_action( 'wp_head', 'customCssInWPHead' );

function customCssInWPHead ()
{?>
    <style>

    </style>
<?php }?>
<?php
function wpdocs_scripts_method() {
    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/assets/js/custom.js', array( 'jquery' ) );

    wp_enqueue_style( 'customCss.css', DIR . '/assets/css/customCss.css', false, filemtime(get_stylesheet_directory().'/assets/css/customCss.css') );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );

function pocketgems_apply_job_check_filter_values($needle = '', $haystack = array()){

  return (count($haystack) == 0 || in_array(trim($needle), $haystack));
}

// Ajax function...
add_action( 'wp_ajax_pocketgems_apply_job_filter', 'pocketgems_apply_job_filter' );
add_action( 'wp_ajax_nopriv_pocketgems_apply_job_filter', 'pocketgems_apply_job_filter' );
function pocketgems_apply_job_filter()
{

    $all_deps_jobs_post_value = $_POST['all_deps_jobs_post_value'];
    $all_deps_jobs_post_value_array = unserialize(base64_decode($all_deps_jobs_post_value));
    $result_jobs = array();
    $output = '';
    if (!is_array($_POST['jobs_filter_roles_checkboxes']) && !is_array($_POST['jobs_filter_teams_checkboxes']) && !is_array($_POST['jobs_filter_games_checkboxes'])) {
        $output='no-filter-applied';
    }else{

    $jobs_filter_roles_checkboxes = (is_array($_POST['jobs_filter_roles_checkboxes'])) ? $_POST['jobs_filter_roles_checkboxes'] : array();
    $jobs_filter_teams_checkboxes = (is_array($_POST['jobs_filter_teams_checkboxes'])) ? $_POST['jobs_filter_teams_checkboxes'] : array();
    $jobs_filter_games_checkboxes = (is_array($_POST['jobs_filter_games_checkboxes'])) ? $_POST['jobs_filter_games_checkboxes'] : array();
    $jobs_filter_locations_checkboxes = (is_array($_POST['jobs_filter_locations_checkboxes'])) ? $_POST['jobs_filter_locations_checkboxes'] : array();
    $jobs_filter_titles_checkboxes = (is_array($_POST['jobs_filter_titles_checkboxes'])) ? $_POST['jobs_filter_titles_checkboxes'] : array();
    if (!count($jobs_filter_roles_checkboxes) &&
        !count($jobs_filter_teams_checkboxes) &&
        !count($jobs_filter_games_checkboxes) &&
        !count($jobs_filter_locations_checkboxes) &&
        !count($jobs_filter_titles_checkboxes)
    ) {
        $result_jobs = $all_deps_jobs_post_value_array;
    } else if (count($jobs_filter_roles_checkboxes) && count($all_deps_jobs_post_value_array)) {
        foreach ($all_deps_jobs_post_value_array as $all_deps_jobs_post_key => $all_deps_jobs_post_value) {
            foreach ($jobs_filter_roles_checkboxes as $jobs_filter_roles_checkbox_key => $jobs_filter_roles_checkbox_value) {
                if (in_array(trim($jobs_filter_roles_checkbox_value), $all_deps_jobs_post_value)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['teams'], $jobs_filter_teams_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['games'], $jobs_filter_games_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['locations'], $jobs_filter_locations_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['titles'], $jobs_filter_titles_checkboxes)
                ) {
                    $result_jobs[$all_deps_jobs_post_key] = $all_deps_jobs_post_value_array[$all_deps_jobs_post_key];
                }
            }
        }
    } //Looking from Team onwards
    else if (count($jobs_filter_teams_checkboxes) && count($all_deps_jobs_post_value_array)) {
        foreach ($all_deps_jobs_post_value_array as $all_deps_jobs_post_key => $all_deps_jobs_post_value) {
            foreach ($jobs_filter_teams_checkboxes as $jobs_filter_teams_checkbox_key => $jobs_filter_teams_checkbox_value) {
                if (in_array(trim($jobs_filter_teams_checkbox_value), $all_deps_jobs_post_value)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['games'], $jobs_filter_games_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['locations'], $jobs_filter_locations_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['titles'], $jobs_filter_titles_checkboxes)
                ) {
                    $result_jobs[$all_deps_jobs_post_key] = $all_deps_jobs_post_value_array[$all_deps_jobs_post_key];
                }
            }
        }
    } //Looking from Games onwards
    else if (count($jobs_filter_games_checkboxes) && count($all_deps_jobs_post_value_array)) {
        foreach ($all_deps_jobs_post_value_array as $all_deps_jobs_post_key => $all_deps_jobs_post_value) {
            foreach ($jobs_filter_games_checkboxes as $jobs_filter_games_checkbox_key => $jobs_filter_games_checkbox_value) {
                if (in_array(trim($jobs_filter_games_checkbox_value), $all_deps_jobs_post_value)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['locations'], $jobs_filter_locations_checkboxes)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['titles'], $jobs_filter_titles_checkboxes)
                ) {
                    $result_jobs[$all_deps_jobs_post_key] = $all_deps_jobs_post_value_array[$all_deps_jobs_post_key];
                }
            }
        }
    } //Looking from Location onwards
    else if (count($jobs_filter_locations_checkboxes) && count($all_deps_jobs_post_value_array)) {
        foreach ($all_deps_jobs_post_value_array as $all_deps_jobs_post_key => $all_deps_jobs_post_value) {
            foreach ($jobs_filter_locations_checkboxes as $jobs_filter_locations_checkbox_key => $jobs_filter_locations_checkbox_value) {
                if (in_array(trim($jobs_filter_locations_checkbox_value), $all_deps_jobs_post_value)
                    && pocketgems_apply_job_check_filter_values($all_deps_jobs_post_value_array[$all_deps_jobs_post_key]['titles'], $jobs_filter_titles_checkboxes)
                ) {
                    $result_jobs[$all_deps_jobs_post_key] = $all_deps_jobs_post_value_array[$all_deps_jobs_post_key];
                }
            }
        }
    } //Looking from Title onwards
    else if (count($jobs_filter_titles_checkboxes) && count($all_deps_jobs_post_value_array)) {
        foreach ($all_deps_jobs_post_value_array as $all_deps_jobs_post_key => $all_deps_jobs_post_value) {
            foreach ($jobs_filter_titles_checkboxes as $jobs_filter_titles_checkbox_key => $jobs_filter_titles_checkbox_value) {
                if (in_array(trim($jobs_filter_titles_checkbox_value), $all_deps_jobs_post_value)) {
                    $result_jobs[$all_deps_jobs_post_key] = $all_deps_jobs_post_value_array[$all_deps_jobs_post_key];
                }
            }
        }
    }

    if (count($result_jobs)) {
        $output = '<div class="job-table-listing-result">';


        $output .= '<div class="padding-container">
            <h3 class="black">Search Results</h3>
            <div class="jobs-table-wrapper">
              <table width="100%" id="search-results">
                <thead>
                <tr class="border_bottom gray">';
        $output .= '<th scope="col">
                  <span class="title">Roles</span>
                </th>
                <th scope="col"><span class="title">Team</span></th>
                <th scope="col"><span class="title">Game</span></th>
                <th scope="col"><span class="title">Location</span></th>
                <th scope="col"><span class="title">Type</span></th>';
        $output .= '
                </tr>
                </thead>';
        foreach ($result_jobs as $result_job) {
            $output .= '<tr class="border_bottom">
                        <td  data-label="Role">
                            <a href="' . $result_job['absolute_url'] . '">' . $result_job['roles'] . '</a>
                        </td>
                        <td data-label="Team">' . $result_job['teams'] . '</td>
                        <td data-label="Game">' . $result_job['games'] . '</td>
                        <td data-label="Location">' . $result_job['locations'] . '</td>
                        <td data-label="Type">' . $result_job['titles'] . '</td>
                    </tr>';
        }
        $output .= '</table>
            </div>
        </div>
      </div>';
    } else {
        $output = '<div class="job-table-msg"> No search results found!</div>';
    }
}
  echo $output;
	wp_die();
}

function pocketgems_get_team_members_id($team_members) {
  $team_members_id = array();
  foreach ($team_members as $team_member) {
    $team_members_id[] = $team_member->ID;
  }
  return $team_members_id;
}

function pocketgems_sort_alphabetic($first_obj, $second_obj){
  return strcmp($first_obj, $second_obj);
}

?>
