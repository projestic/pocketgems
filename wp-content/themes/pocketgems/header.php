<!DOCTYPE html>
<html class="mobile-menu-active">
    <head profile="http://gmpg.org/xfn/11">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />

        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <title><?php bloginfo('name'); ?><?php wp_title( '-', true, 'left' ) ?></title

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo DIR; ?>/assets/images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo DIR; ?>/assets/images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo DIR; ?>/assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo DIR; ?>/assets/images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo DIR; ?>/assets/images/favicon/favicon-16x16.png">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo DIR; ?>/assets/images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <?php wp_head(); ?>
        <script type="text/javascript">
            var adminAjax = "<?php echo admin_url('admin-ajax.php'); ?>";
        </script>
    </head>
    <body <?php body_class(); ?>>


    <div class="layout">

        <header id="header" class="header header-sticky">
            <div class="holder">
                <div class="logo-holder">
                    <a class="site-logo" href="<?= esc_url(home_url('/')); ?>" title="<?php echo get_bloginfo('name'); ?>">
                        <img class="hidden-retina" src="<?php echo DIR; ?>/assets/images/pocket-gems-logo.png" alt="<?php bloginfo('name'); ?>">
                        <img class="visible-retina" src="<?php echo DIR; ?>/assets/images/pocket-gems-logo@2x.png" alt="<?php bloginfo('name'); ?>">
                    </a>

                    <a href="#" class="menu-btn bt-menu-trigger"><span>Menu</span></a>
                </div>

                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array(
                        'container_class' => 'nav-holder',
                        'theme_location' => 'primary_navigation',
                        'menu' => 'Main Menu',
                        'menu_class' => 'menu menu-primary main-nav'));
                endif;
                ?>
            </div>


            <?php
            if (has_nav_menu('secondary_main_menu')) :
                wp_nav_menu(array(
                    'container_class' => 'secondary-main-menu',
                    'theme_location' => 'secondary_main_menu',
                    ));
            endif;
            ?>
        </header>

        <script>
            // $('a[href*="#"]')
            // .not('[href="#"]')
            // .not('[href="#0"]')
            // .click(function(event) {
            //     if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            //         location.hostname == this.hostname) {
            //         var target = $(this.hash);
            //         target = target.length ? target :
            //                  $('[name=' + this.hash.slice(1) + ']');
            //         if (target.length) {
            //             event.preventDefault();
            //             $('html, body')
            //             .animate(
            //                 { scrollTop: target.offset().top },
            //                 1000,
            //                 function() {
            //                     var $target = $(target);
            //                     $target.focus();
            //                     if ($target.is(":focus")) {
            //                         return false;
            //                     } else {
            //                         $target.attr('tabindex','-1');
            //                         $target.focus();
            //                     };
            //                 }
            //             );
            //         }
            //     }
            // });
        </script>

        <script>
            $( document ).ready(function() {
               $('.page-id-10 #overview').removeAttr('id');
               $('.page-id-10 .page-header').attr('id', 'overview');
               $("#header").hide();
               $("#header").delay(300).slideDown('medium');
                $(".page-id-10").attr('id', 'top');
               $(".page-id-10 .menu-item-21 > a").attr('id', 'top');
               $(".page-id-10 .menu-item-21 > a").attr("href", "#top")
               $("a[href='#top']").click(function() {
                  $("html, body").animate({ scrollTop: 0 }, "slow");
                  return false;
                });
     $('.bt-menu-trigger').click(function(e) {
            $(".searchFilters").hide();
     });





        $(".showFilter").click(function(e) {
          //$(this).parent('.box').addClass('active'); .toggleClass( "highlight" )
          $(this).parent('.box').toggleClass('active').siblings().removeClass('active');
          e.preventDefault();
        });
        $(document).click(function(event) {
            if(!$(event.target).closest('.box').length) {
                if($('.box').is(":visible")) {
                    $('.box').removeClass('active');
                }
            }
        });
     $('#btnSeaerchFilters').click(function(e) {
        var link = $(this);
        $('.searchFilters .jobfilters1').slideToggle('slow', function() {
            if ($(this).is(':visible')) {
                 link.text('Close Search Filters');
                 $(this).css( 'display', 'flex' );
            } else {
                 link.text('Open Search Filters');
                 $(this).removeAttr( 'style' );
            }
        });
              e.preventDefault();


      });
     if ($(window).width() > 640) {
        $('#menu2').addClass('f-nav');
    } else {
        $('#menu2').removeClass('f-nav');
    }

    });
        </script>
