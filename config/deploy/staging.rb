############################################
# Setup Server
############################################
# server 'ec2-52-53-116-27.us-west-1.compute.amazonaws.com', roles: [:web, :app, :db, :resque_worker], primary: true
# # ssh @
# set :repo_url,        'git@github.com:punchagency/research-show.git'
# set :application,     'pocketgems-website'
# set :user,            'abdul'
# set :puma_threads,    [4, 16]
# set :puma_workers,    1
#
# set :branch, "development"
# set :stage, :staging
# set :stage_url, "http://staging.getcliffhanger.com"
# server "staging.getcliffhanger.com", user: "abdul", roles: %w{web app db}
# set :deploy_to, "/home/#{fetch(:user)}/#{fetch(:application)}"
#
# # set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
# # set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
# # set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
# # set :puma_access_log, "#{release_path}/log/puma.error.log"
# # set :puma_error_log,  "#{release_path}/log/puma.access.log"
# set :ssh_options,     { user: fetch(:user) }
# # set :puma_preload_app, true
# # set :puma_worker_timeout, nil
# # set :puma_init_active_record, true  # Change to false when not using ActiveRecord
# set :nginx_use_ssl, false
# set :nginx_server_name, 'staging.pocketgems.com'
# #set :nginx_socket_flags, 'research_show-puma.sock'
# set :nginx_config_name, 'pocketgems-website'


















set :branch, 'master'

# The environment's name. To be used in commands and other references.
set :stage, :staging
set :application,     'pocketgems-website'
set :user,            'abdul'
set :log_level,     :debug
# The URL of the website in this environment.
set :stage_url, 'http://staging.pocketgems.com/'

# The environment's server credentials
set :use_sudo, true
# set :rvm1_ruby_version, 'ruby-2.4.0'
# set :bundle_path, nil

server 'staging.pocketgems.com', user: 'abdul', roles: %w(web app db)
set :deploy_to, "/home/#{fetch(:user)}/#{fetch(:application)}"


# The deploy path to the website on this environment's server.


############################################
# Setup Git
############################################


#
# append :linked_files, ".env"

############################################
# Extra Settings
############################################

#specify extra ssh options:

#set :ssh_options, {
#    auth_methods: %w(password),
#    password: 'password',
#    user: 'username',
#}

#specify a specific temp dir if user is jailed to home
#set :tmp_dir, "/path/to/custom/tmp"
