<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'focustec_pocketgems');

/** MySQL database username */
define('DB_USER', 'focustec_wp');

/** MySQL database password */
define('DB_PASSWORD', 'abcd!@#$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
// define('AUTH_KEY',         'put your unique phrase here');
// define('SECURE_AUTH_KEY',  'put your unique phrase here');
// define('LOGGED_IN_KEY',    'put your unique phrase here');
// define('NONCE_KEY',        'put your unique phrase here');
// define('AUTH_SALT',        'put your unique phrase here');
// define('SECURE_AUTH_SALT', 'put your unique phrase here');
// define('LOGGED_IN_SALT',   'put your unique phrase here');
// define('NONCE_SALT',       'put your unique phrase here');
define('AUTH_KEY',         'kL`}|^D<aeni66K3uOo}R-<?6Yi57KeY`v Ec]|o.IF<Xg9+RhEsK.vs)Pv[bU2+');
define('SECURE_AUTH_KEY',  'd_>8Cf-V<hYm5[O8WPeQCiE|Bb0KQm..#`SY&V%%Pqg?|q4rkAIwBa-]ol%!Gs# ');
define('LOGGED_IN_KEY',    'm|>$-WChN*!-RE]sB|wl4VQtG.XVe}.mbcaZ?=V#~}$ucKA.lG?i+,;UB?8<Ai|;');
define('NONCE_KEY',        'o7Z++f]6fYOF%CM?D-Hz{yq6k5;na|m^_Uw_9(<Vu|M+s1_eSyML-mU~nAN+kGm(');
define('AUTH_SALT',        'q-fe4$cm=y+-gB~yXXM7.IB`_+R-ya+.)u-):(m}[*5Znn:J>,09>_GwXp+w#{R ');
define('SECURE_AUTH_SALT', 'UO4`<{.wJG$2h^r1ng6gF7NIkW_-T`dA#F0>v-U2!p>$Rk&u$*3cB_Om_POO-[+3');
define('LOGGED_IN_SALT',   'cycf MJt mO=__3-|w>pld]#WU^_,tsGm,u1S9?xdcG^1|9^RJcDfgQ@wB/F:&s[');
define('NONCE_SALT',       'mV4##X2E>TEMXh66-e>1Z@vzX)/U_NV[%N@%K3x2A^^a}|)v4Qm+4lv,J,l-qJw$');

// define('WP_MEMORY_LIMIT', '128M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
